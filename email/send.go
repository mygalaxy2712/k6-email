package email

import (
	"bytes"
	"crypto/tls"
	"fmt"
	"text/template"

	metricName "go.k6.io/k6/lib/metrics"
	"go.k6.io/k6/stats"
	gomail "gopkg.in/mail.v2"
)

type EmailBody struct {
	DataReceived          string
	DataSent              string
	HttpReqBlocked        string
	HttpReqConnection     string
	HttpReqDuration       string
	HttpReqFailed         string
	HttpReqReceiving      string
	HttpReqSending        string
	HttpReqTlsHandshaking string
	HttpReqWaiting        string
	HttpReqs              string
	IterationDuration     string
	Iterations            string
	Vus                   string
	VusMax                string
}

func formatMetric(metric *stats.Metric) string {
	sink := metric.Sink
	switch s := sink.(type) {
	case *stats.GaugeSink:
		return fmt.Sprintf("%d, Min: %d, Max: %d", int(s.Value), int(s.Min), int(s.Max))
	case *stats.CounterSink:
		return fmt.Sprintf("%d", int(s.Value))
	case *stats.TrendSink:
		return fmt.Sprintf("Avg: %.2fs, Min: %.2fs, Med: %.2fs, Max: %.2fs", s.Avg/1000, s.Min/1000, s.Med/1000, s.Max/1000)
	case *stats.RateSink:
		return fmt.Sprintf("Total: %d, Failed: %d", s.Total, s.Trues)
	default:
		return ""
	}
}

func SendMail(to string, metrics map[string]*stats.Metric) {
	//Mail server
	username := ""
	password := ""
	mailhost := "localhost"
	mailport := 1025
	// username := "0a8f9f5b5fae0d"
	// password := "5ad08cbb7d99ef"
	// mailhost := "smtp.mailtrap.io"
	// mailport := 25

	//Mail content
	from := "no-reply@k6.test.com"
	subject := "[K6] Testing Result Summary"

	//Get mail template
	t, err := template.ParseFiles("email/template.html")

	if err != nil {
		fmt.Println(err)
		return
	}

	//Binding data
	var body bytes.Buffer
	body.Write([]byte(""))
	t.Execute(&body, EmailBody{
		DataReceived:          formatMetric(metrics[metricName.DataReceivedName]),
		DataSent:              formatMetric(metrics[metricName.DataSentName]),
		HttpReqBlocked:        formatMetric(metrics[metricName.HTTPReqBlockedName]),
		HttpReqConnection:     formatMetric(metrics[metricName.HTTPReqConnectingName]),
		HttpReqDuration:       formatMetric(metrics[metricName.HTTPReqDurationName]),
		HttpReqFailed:         formatMetric(metrics[metricName.HTTPReqFailedName]),
		HttpReqReceiving:      formatMetric(metrics[metricName.HTTPReqReceivingName]),
		HttpReqSending:        formatMetric(metrics[metricName.HTTPReqSendingName]),
		HttpReqTlsHandshaking: formatMetric(metrics[metricName.HTTPReqTLSHandshakingName]),
		HttpReqWaiting:        formatMetric(metrics[metricName.HTTPReqWaitingName]),
		HttpReqs:              formatMetric(metrics[metricName.HTTPReqsName]),
		IterationDuration:     formatMetric(metrics[metricName.IterationDurationName]),
		Iterations:            formatMetric(metrics[metricName.IterationsName]),
		Vus:                   formatMetric(metrics[metricName.VUsName]),
		VusMax:                formatMetric(metrics[metricName.VUsMaxName]),
	})

	//Init sender
	m := gomail.NewMessage()

	// Set E-Mail sender
	m.SetHeader("From", from)

	// Set E-Mail receivers
	m.SetHeader("To", to)

	// Set E-Mail subject
	m.SetHeader("Subject", subject)

	// Set E-Mail body. You can set plain text or html with text/html
	m.SetBody("text/html", string(body.Bytes()))

	// Settings for SMTP server
	d := gomail.NewDialer(mailhost, mailport, username, password)

	// This is only needed when SSL/TLS certificate is not valid on server.
	// In production this should be set to false.
	d.TLSConfig = &tls.Config{InsecureSkipVerify: true}

	// Now send E-Mail
	if err := d.DialAndSend(m); err != nil {
		fmt.Println(err)
		panic(err)
	}

	return
}
