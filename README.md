# Email for K6
## Required
- Docker
- Golang

## Setup
1. Run docker compose
> docker-compose up
2. Init scipt for testing

## Run
>  go run main.go run script.js --report-to={email}

## Features
1. Mailserver: http://localhost:8025
2. Grafana: http://localhost:3000
3. Influxdb: http://localhost:8086